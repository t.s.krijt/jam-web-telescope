# Jam Web Telescope

## How to build
Open with visual studio 2022. Make sure that the neccecary nugget packages are installed. (google-or tools).
ctrl+shift+B

## How to run experiments.
### Direct input instances
Run the program with either the '-online' argument or the '-offline' argument.
By default offline is selected.
Input your instance according to the format provide in https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56334664_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%282%29.pdf

### Visual Studio 2022
Open the test explorer. (ctrl+e,t)
Make sure all tests are selected. 
Click on run all tests.

### Commandline
Run the generated app with the '-testcomp' argument.