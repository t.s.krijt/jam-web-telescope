using JamWebTelescope;
using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.Algorithms.Online;
using JamWebTelescope.entities;
using NUnit.Framework;

namespace Testescope
{
	public class SimpleTests
	{

		[DatapointSource]
#pragma warning disable IDE0052 // Remove unread private members
		readonly int[] possibleInts = { 0, 1, 2, 5 };

		[DatapointSource]
		readonly decimal[] possibledecimals = { 0, 1, 2, 5, 0.618m, 10, 6, 3.14m, 16, 42, 69 };
#pragma warning restore IDE0052 // Remove unread private members

		[Test]
		public void ExampleCaseWithoutBlackouts()
		{
			int imageCount = 5;
			decimal[] imageLengths = new decimal[5] { 0.618m, 2, 10, 6, 3.14m };
			Timeline result1 = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, 0, Array.Empty<decimal>());
			
			Timeline onlineResult = TestHelpers.CreateBaseTimeline(0, Array.Empty<decimal>(), imageCount);
			OnlineAlgorithms.SimpleOnlineAlgorithm(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(result1, imageCount, imageLengths);
			Assert.That(result1, Has.Length.EqualTo(imageLengths.Sum()));
			Assert.That(result1, Has.Length.EqualTo(onlineResult.Length));
		}


		[Test]
		public void ExampleCase()
		{
			// See /BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling (1).pdf in blackboard
			int imageCount = 5;
			decimal[] imageLengths = new decimal[5] { 0.618m, 2, 10, 6, 3.14m };

			int blackoutCount = 2;
			decimal[] blackoutStartAndLength = new decimal[4] { 4, 0.25m, 5, 11 };

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			OnlineAlgorithms.SimpleOnlineAlgorithm(imageCount, imageLengths, onlineResult);

			Timeline result1 = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			
			if (onlineResult.Length < result1.Length)
				Assert.Fail();

			TestHelpers.ValidateTimeline(result1, imageCount, imageLengths);
		}
	}
}