using JamWebTelescope;
using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.Algorithms.Online;
using JamWebTelescope.entities;
using NUnit.Framework;

namespace Testescope
{
	public class OnlineTests
	{
		[DatapointSource]
#pragma warning disable IDE0052 // Remove unread private members
		readonly int[] possibleInts = { 0, 1, 2, 5 };

		[DatapointSource]
		readonly decimal[] possibledecimals = { 0, 1, 2, 5, 0.618m, 10, 6, 3.14m, 16, 42, 69 };

		[DatapointSource]
		readonly Action<int, decimal[], IOnlineTimeline>[] possibleAlgorithms =
			{
			OnlineAlgorithms.SimpleOnlineAlgorithm,
			OnlineAlgorithms.BiggestFirst,
			OnlineAlgorithms.SmallestFirst,
		};
#pragma warning restore IDE0052 // Remove unread private members

		[Test]
		public void ExampleCaseWithoutBlackouts()
		{
			int imageCount = 5;
			decimal[] imageLengths = new decimal[5] { 0.618m, 2, 10, 6, 3.14m };
			Timeline result = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, 0, Array.Empty<decimal>());

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(0, Array.Empty<decimal>(), imageCount);
			OnlineAlgorithms.SimpleOnlineAlgorithm(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
			Assert.That(result, Has.Length.EqualTo(imageLengths.Sum()));
			Assert.That(result, Has.Length.EqualTo(onlineResult.Length));
		}


		[Test]
		public void ExampleCase()
		{
			// See /BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling (1).pdf in blackboard
			int imageCount = 5;
			decimal[] imageLengths = new decimal[5] { 0.618m, 2, 10, 6, 3.14m };

			int blackoutCount = 2;
			decimal[] blackoutStartAndLength = new decimal[4] { 4, 0.25m, 5, 11 };

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			OnlineAlgorithms.SimpleOnlineAlgorithm(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		[Test, Theory]
		public void RandomTestCases(int imageCount, int blackoutCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Unit-size images: s_j = 1 for all j
		[Test, Theory]
		public void UnitSizeImages(int imageCount, int blackoutCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = new decimal[imageCount];
			for (int i = 0; i < imageCount; i++)
			{
				imageLengths[i] = 1;
			}

			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		//Bounded-size images: s_j \elem [s_min, s_max] for some constants s_min and s_max
		static Dictionary<string, decimal> cUpperBoundBoundedSizeImages = new();
		[Test, Theory]
		public void BoundedSizeImages(int imageCount, int blackoutCount, decimal sMinBound, decimal sMaxBound, Action<int, decimal[], IOnlineTimeline> f)
		{
			if (sMaxBound < sMinBound)
				Assert.Ignore();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount, sMinBound, sMaxBound);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cUpperBoundBoundedSizeImages, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Only one unavailable interval: m = 1
		[Test, Theory]
		public void OnlyOneBlackout(int imageCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(1);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(1, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic unavailable intervals: for all i, the i-th unavailable interval has t_i = i * p for some p
		[Test, Theory]
		public void BlackoutsWithInterfalls(int imageCount, int blackoutCount, decimal p, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic and regular unavailable intervals: for all i, the i-th unavailable interval has ti = i * p for some p and l_i = l
		[Test, Theory]
		public void BlackoutsWithInterfallsAndLengths(int imageCount, int blackoutCount, decimal p, decimal l, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p, l);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Bounded-length unavailable intervals: for all i, the i-th unavailable inter-val has l_i \elem [l_min, l_max] for some constants l_min and l_max
		[Test, Theory]
		public void BoundedLengthBlackouts(int imageCount, int blackoutCount, decimal lowerbound, decimal upperbound, Action<int, decimal[], IOnlineTimeline> f)
		{
			if (upperbound < lowerbound)
				Assert.Ignore();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount, lowerbound, upperbound);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);
		}
	}
}