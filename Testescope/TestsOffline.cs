using JamWebTelescope;
using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.entities;
using NUnit.Framework;

namespace Testescope
{
	public class OfflineTests
	{

		[DatapointSource]
#pragma warning disable IDE0052 // Remove unread private members
		readonly int[] possibleInts = { 0, 1, 2, 5, };

		[DatapointSource]
		readonly decimal[] possibledecimals = { 0, 1, 2, 5, 0.618m, 10, 6, 3.14m, 16, 42, 69 };

		[DatapointSource]
		readonly Func<int, decimal[], int, decimal[], Timeline>[] possibleAlgorithms =
			{
			//OfflineAlgorithms.FindOptimalSolutionWithPermutations,
			//OfflineAlgorithms.BiggestBlockFirst,
			OfflineAlgorithms.SearchWithLinearProgramming, 
			//OfflineAlgorithms.SearchWithLinearProgrammingGLOP,
		};
#pragma warning restore IDE0052 // Remove unread private members

		[Test, Theory]
		public void RandomTestCases(int imageCount, int blackoutCount, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Unit-size images: s_j = 1 for all j
		[Test, Theory]
		public void UnitSizeImages(int imageCount, int blackoutCount, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			decimal[] imageLengths = new decimal[imageCount];
			for (int i = 0; i < imageCount; i++)
			{
				imageLengths[i] = 1;
			}

			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		//Bounded-size images: s_j \elem [s_min, s_max] for some constants s_min and s_max
		[Test, Theory]
		public void BoundedSizeImages(int imageCount, int blackoutCount, decimal sMinBound, decimal sMaxBound, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			if (sMaxBound < sMinBound)
				Assert.Ignore();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount, sMinBound, sMaxBound);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Only one unavailable interval: m = 1
		[Test, Theory]
		public void OnlyOneBlackout(int imageCount, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(1);

			Timeline result = f(imageCount, imageLengths, 1, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, 1, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic unavailable intervals: for all i, the i-th unavailable interval has t_i = i * p for some p
		[Test, Theory]
		public void BlackoutsWithInterfalls(int imageCount, int blackoutCount, decimal p, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic and regular unavailable intervals: for all i, the i-th unavailable interval has ti = i * p for some p and l_i = l
		[Test, Theory]
		public void BlackoutsWithInterfallsAndLengths(int imageCount, int blackoutCount, decimal p, decimal l, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p, l);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Bounded-length unavailable intervals: for all i, the i-th unavailable inter-val has l_i \elem [l_min, l_max] for some constants l_min and l_max
		[Test, Theory]
		public void BoundedLengthBlackouts(int imageCount, int blackoutCount, decimal lowerbound, decimal upperbound, Func<int, decimal[], int, decimal[], Timeline> f)
		{
			if (upperbound < lowerbound)
				Assert.Ignore();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount, lowerbound, upperbound);

			Timeline result = f(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);

			Timeline optimalSolution = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			Assert.That(Math.Round(result.Length, 5), Is.EqualTo(Math.Round(optimalSolution.Length, 5)));

			TestHelpers.ValidateTimeline(result, imageCount, imageLengths);
		}
	}
}