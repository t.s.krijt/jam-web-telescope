﻿namespace JamWebTelescope
{
	public class Transmission : Event
	{
		public bool Failed { get; set; }

		public bool IsSend { get; set; }

		public int Index => index;

		private readonly int index;

		// Start doesn't say anything for transmissions beceause it still needs to be set!!	
		public Transmission(int index, decimal length) : base(0, length)
		{
			this.index = index;
			Failed = false;
		}

		public override string ToString()
		{
			return "T: [" + Start + ", " + Length + "]";
		}

		public void SetStart(decimal start)
		{
			this.start = start;
		}

		public Transmission Clone()
		{
			Transmission clone = new Transmission(Index, Length);
			clone.SetStart(Start);
			clone.Failed = Failed;
			clone.IsSend = IsSend;
			return clone;
		}
	}
}
