﻿namespace JamWebTelescope
{
	public class Event : IComparable<Event>
	{
		public decimal Start => start;
		public decimal Length => length;
		public decimal End
		{
			get { return Start + Length; }
			set
			{
				length = value - start;
			}
		}

		public override string ToString()
		{
			return "[" + Start + ", " + Length + "]";
		}


		protected decimal start;
		private decimal length;


		public Event(decimal start, decimal length)
		{
			this.start = start;
			this.length = length;
		}

		public bool Overlap(Event? nextEvent)
		{
			if (nextEvent == null) return false;

			// Edge cases for when length is equal to 0
			if (length == 0)
			{
				if (End == nextEvent.Start)
					return false;

				if (Start < nextEvent.End && Start > nextEvent.Start)
					return true;
			}


			if (Start <= nextEvent.Start && End > nextEvent.Start)
				return true;

			if (nextEvent.Start <= Start && nextEvent.End > Start)
				return true;


			return false;
		}

		public int CompareTo(Event? other)
		{
			if (other == null) throw new ArgumentNullException(nameof(other));

			decimal difference = start - other.Start;

			if (difference < 0) return -1;
			if (difference > 0) return 1;

			// If the start is equal but the one with the shortes length first 
			// This is beceause we want events with length 0 to always be first.
			if (other.length > length) return -1;
			if (other.length < length) return 1;

			return 0;
		}
	}
}
