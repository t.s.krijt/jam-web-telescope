﻿namespace JamWebTelescope
{
	public class Blackout : Event
	{
		public Blackout(decimal start, decimal length) : base(start, length)
		{

		}

		public override string ToString()
		{
			return "B: [" + Start + ", " + Length + "]";
		}

		public void Merge(Blackout other)
		{
			// Assume we are the first one and we overlap.
			if (!Overlap(other) || start > other.start)
				throw new Exception();

			End = other.End;
		}
	}
}
