﻿namespace JamWebTelescope.entities
{
	public class Timeline : IOnlineTimeline
	{
		public bool Valid = true;

		public decimal Length
		{
			get
			{
				for (int i = events.Count - 1; i >= 0; i--)
				{
					if (events[i].GetType() == typeof(Transmission))
					{
						return events[i].End;
					}
				}
				return 0;
			}
		}

		private int transmissionCount = 0;

		public int TransmissionCount => transmissionCount;

		readonly List<Event> events;

		public Timeline(List<Blackout> blackouts, int capacity)
		{
			events = new List<Event>(capacity + 1);
			if (blackouts != null && blackouts.Any())
			{
				//events.Add(new Blackout(0, 0));

				blackouts.Sort();
				// Merge overlapping blackouts
				for (int i = 0; i < blackouts.Count - 1; i++)
				{
					if (blackouts[i].Overlap(blackouts[i + 1]))
					{
						blackouts[i].Merge(blackouts[i + 1]);
						blackouts.RemoveAt(i + 1);
						i--;
					}
				}


				// Add blackouts
				foreach (Event blackout in blackouts)
				{
					events.Add(blackout);
				}
				events.Sort();
			}
		}

		/// <summary>
		/// The last blackout that impacts the score
		/// </summary>
		/// <returns>Returns the index in the events list of the last blackout that impacts the score.</returns>
		public int GetLastBlackout()
		{
			bool foundLastTransmission = false;
			for (int i = events.Count - 1; i >= 0; i--)
			{
				if (events[i].GetType() == typeof(Transmission))
					foundLastTransmission = true;
				else if (events[i].GetType() == typeof(Blackout))
					if (foundLastTransmission)
						return i;
			}
			return -1;
		}

		public int GetBlackoutIndex(int index)
		{
			int blackoutCounter = 0;
			for (int i = 0; i < events.Count; i++)
			{
				if (events[i].GetType() == typeof(Blackout))
				{
					if (blackoutCounter == index)
						return i;
					blackoutCounter++;
				}
			}

			return events.Count;
		}

		public decimal GetTimeBeforeEvent(int i)
		{
			if (i == events.Count)
				return decimal.MaxValue;

			if (i != 0)
				return events[i].Start - events[i - 1].End;

			return events[i].Start;
		}

		public int EnoughOfTimeBetweenEvents(decimal time)
		{
			decimal sum = events[0].Start;

			for (int i = 1; i < events.Count; i++)
			{
				if (sum > time)
					return i;
				sum += events[i].End - events[i - 1].Start;
			}
			return events.Count;
		}

		public string[] GetPrintValues(int transmissionCount)
		{
			string[] outputs = new string[transmissionCount + 1];


			// Las value we output is the final score.
			outputs[0] = Length.ToString();

			foreach (Event e in events)
			{
				if (e.GetType() == typeof(Transmission))
				{
					Transmission t = (Transmission)e;

					// Only set as output if the transmission was send.
					if (t.IsSend)
						outputs[t.Index + 1] = (t.Start.ToString());
				}
			}

			return outputs;
		}

		public void Validate()
		{
			// If events overlap in the timeline, this timeline isn't valid!!!
			for (int i = 0; i < events.Count - 1; i++)
			{
				if (events[i].Overlap(events[i + 1]))
					throw new Exception("Events overlap!");
			}
		}

		public void AddTransmissions(List<Transmission> transmissions)
		{
			foreach (Transmission t in transmissions)
				AddTransmission(t);
		}

		public void AddTransmissionsBefore(List<Transmission> transmissions, int index)
		{
			foreach (Transmission t in transmissions)
				AddTransmissionBefore(t, index++);
			events.Sort();
		}

		public void AddTransmissionBefore(Transmission transmission, int index)
		{
			if (index == 0)
			{
				transmission.SetStart(0);
				events.Insert(0, transmission);
				return;
			}

			transmission.SetStart(events[index - 1].End);
			events.Insert(index, transmission);
		}

		public void AddTransmission(Transmission transmission)
		{
			transmission.SetStart(0);

			// If there're no events, just add the transmission at the start of the timeline.
			if (events.Count != 0)
			{
				// First position to try is at the start.

				// Check if it fits.
				bool foundPosition = !transmission.Overlap(events[0]);

				if (!foundPosition)
				{
					for (int i = 0; i < events.Count - 1; i++)
					{
						transmission.SetStart(events[i].End);

						// Check if it fits at this position.
						if (!transmission.Overlap(events[i + 1]))
						{
							//Add it to the first position it fits at.
							foundPosition = true;
							break;
						}
					}
				}

				// Just add it add the end.
				if (!foundPosition)
				{
					transmission.SetStart(events.Last().End);
				}
			}

			transmission.IsSend = true;

			// Add the event to the list.
			events.Add(transmission);

			// Sort the list.
			events.Sort();

			// Validate that it's still correct.
			Validate();

			// We know we have one more transmission
			transmissionCount++;

			// Done!
			return;
		}

		public void AddTransmission(Transmission transmission, double startTime)
		{
			transmission.SetStart((decimal)startTime);

			transmission.IsSend = true;

			// Add the event to the list.
			events.Add(transmission);

			// Sort the list.
			events.Sort();

			// Validate that it's still correct.
			//Validate();

			// We know we have one more transmission
			transmissionCount++;

			// Done!
			return;
		}

		public void Add(Event newEvent)
		{
			events.Add(newEvent);
			events.Sort();
		}

		decimal currentPosition = 0;
		public bool Step()
		{
			// We assume that the timeline is sorted!
			for (int i = 0; i < events.Count; i++)
			{
				if (events[i].Start > currentPosition)
				{
					currentPosition = events[i].Start;
					return true;
				}

				if (events[i].End > currentPosition)
				{
					currentPosition = events[i].End;
					return true;
				}
			}
			return false;
		}

		public bool TryToSend(Transmission transmission)
		{
			transmission.SetStart(currentPosition);
			events.Add(transmission);
			events.Sort();

			for (int i = 0; i < events.Count; i++)
			{
				if (events[i] == transmission)
				{
					if (i + 1 < events.Count && transmission.Overlap(events[i + 1]))
					{
						// The transmission will fail :(
						transmission.End = events[i + 1].Start;
						transmission.Failed = true;
						return false;
					}
					else
					{
						// The transmission will succeed! :)
						transmission.IsSend = true;
						return true;
					}
				}
			}

			throw new Exception("You shouldn't beable to come here");
		}

		public bool CanSend()
		{
			// We can send if there are no events currently happening
			// We assume that the timeline is sorted!
			for (int i = 0; i < events.Count; i++)
			{
				if (events[i].Start > currentPosition)
				{
					return true;
				}

				if (events[i].End > currentPosition)
				{
					return false;
				}
			}

			return true;
		}
	}
}
