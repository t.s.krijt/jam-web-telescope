﻿namespace JamWebTelescope.entities
{
	public interface IOnlineTimeline
	{
		public bool Step();
		public bool CanSend();
		public bool TryToSend(Transmission transmission);
	}
}
