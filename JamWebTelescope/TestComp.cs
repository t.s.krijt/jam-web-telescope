﻿using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.Algorithms.Online;
using JamWebTelescope.entities;

namespace JamWebTelescope
{
	internal static class TestComp
	{
		private static void PrintColour(string text, ConsoleColor color)
		{
			var prev = Console.ForegroundColor;
			Console.ForegroundColor = color;
			Console.WriteLine(text);
			Console.ForegroundColor = prev;
		}

		public static void PrintGreen(string text)
		{
			PrintColour(text, ConsoleColor.Green);
		}

		public static void PrintRed(string text)
		{
			PrintColour(text, ConsoleColor.Red);
		}

		public static void Print()
		{
			foreach (var e in experimentsResults)
			{
				Console.Write(e.Key);
				foreach (string s in e.Value)
					Console.Write(" & " + s);
				Console.WriteLine("\\\\ ");
			}

			Console.WriteLine();

			PrintGreen("Unit-size images: s_j = 1 for all j");
			Console.WriteLine(nameof(cLowerBoundUnitSizeImages) + ":");
			foreach (var keyValue in cLowerBoundUnitSizeImages)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Bounded-size images: s_j \\elem [s_min, s_max] for some constants s_min and s_max");
			Console.WriteLine(nameof(cLowerBoundBoundedSizeImages) + ":");
			foreach (var keyValue in cLowerBoundBoundedSizeImages)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Only one unavailable interval: m = 1");
			Console.WriteLine(nameof(cLowerBoundOneBlackout) + ":");
			foreach (var keyValue in cLowerBoundOneBlackout)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Bounded-length unavailable intervals: for all i, the i-th unavailable inter-val has l_i \\elem [l_min, l_max] for some constants l_min and l_max");
			Console.WriteLine(nameof(cLowerBoundBoundedLengthBlackouts) + ":");
			foreach (var keyValue in cLowerBoundBoundedLengthBlackouts)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Periodic and regular unavailable intervals: for all i, the i-th unavailable interval has ti = i * p for some p and l_i = l");
			Console.WriteLine(nameof(cLowerBoundBlackoutsWithInterfallsAndLengths) + ":");
			foreach (var keyValue in cLowerBoundBlackoutsWithInterfallsAndLengths)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Periodic unavailable intervals: for all i, the i-th unavailable interval has t_i = i * p for some ");
			Console.WriteLine(nameof(cLowerBoundBlackoutsWithInterfalls) + ":");
			foreach (var keyValue in cLowerBoundBlackoutsWithInterfalls)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}

			PrintGreen("Just random...");
			Console.WriteLine(nameof(cLowerRandomTestCases) + ":");
			foreach (var keyValue in cLowerRandomTestCases)
			{
				Console.WriteLine(keyValue.Key + ": " + keyValue.Value);
			}
		}

		readonly static int[] possibleInts = { 0, 1, 2, 5, 7, 9, 15 };
		readonly static decimal[] possibledecimals = { 0, 1, 2, 5, 0.618m, 10, 42, 69, 100, };

		readonly static Action<int, decimal[], IOnlineTimeline>[] onlineFunctions =
		{
			OnlineAlgorithms.BiggestFirst,
			OnlineAlgorithms.SmallestFirst,
			OnlineAlgorithms.AlternateBigSmall,
			OnlineAlgorithms.AlwaysAlternateBigSmall,
		};

		private static Dictionary<string, string[]> experimentsResults = new();
		public static void TestCompetitiveness()
		{
			(float, float) result;
			for (int f = 0; f < onlineFunctions.Length; f++)
			{
				result = BlackoutsWithInterfallsAndLengths(5, 200, 80, 20, onlineFunctions[f]);
				string experimentNameA = "BWIAL " + 5 + " " + 200 + " " + 80 + " " + 20;
				ExperimentHelper(experimentNameA, f, result);

				foreach (int i in possibleInts)
				{
					if (i >= 7)
						continue;

					result = OnlyOneBlackout(i, onlineFunctions[f]);
					ExperimentHelper("OOB " + i, f, result);

					foreach (int j in possibleInts)
					{
						if (i == 0)
							continue;

						if (i > j)
							continue;

						if (j < 5)
							continue;

						result = UnitSizeImages(i, j, onlineFunctions[f]);
						ExperimentHelper("USI " + i + " " + j, f, result);

						result = RandomTestCases(i, j, onlineFunctions[f]);
						ExperimentHelper("RTC " + i + " " + j, f, result);

						foreach (decimal d in possibledecimals)
						{
							result = BlackoutsWithInterfalls(i, j, d, onlineFunctions[f]);
							ExperimentHelper("BWIF " + i + " " + j + " " + d, f, result);

							foreach (decimal e in possibledecimals)
							{
								result = BlackoutsWithInterfallsAndLengths(i, j, d, e, onlineFunctions[f]);
								ExperimentHelper("BWIAL " + i + " " + j + " " + d + " " + e, f, result);

								if (d < e)
									continue;
								result = BoundedSizeImages(i, j, e, d, onlineFunctions[f]);
								ExperimentHelper("BSI " + i + " " + j + " " + d + " " + e, f, result);

								result = BoundedLengthBlackouts(i, j, e, d, onlineFunctions[f]);
								ExperimentHelper("BLB " + i + " " + j + " " + d + " " + e, f, result);

							}
						}
					}
				}
			}
		}

		public static void ExperimentHelper(string name, int f, (float, float) result)
		{

			if (experimentsResults.ContainsKey(name))
			{
				experimentsResults[name][f + 1] = result.Item1.ToString();
			}
			else
			{
				string[] tmp = new string[onlineFunctions.Length + 1];
				tmp[0] = result.Item2.ToString();
				tmp[f + 1] = result.Item1.ToString();
				experimentsResults.Add(name, tmp);
			}
		}


		public static Dictionary<string, decimal> cLowerRandomTestCases = new();
		public static (float, float) RandomTestCases(int imageCount, int blackoutCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerRandomTestCases, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Unit-size images: s_j = 1 for all j
		public static Dictionary<string, decimal> cLowerBoundUnitSizeImages = new();
		public static (float, float) UnitSizeImages(int imageCount, int blackoutCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = new decimal[imageCount];
			for (int i = 0; i < imageCount; i++)
			{
				imageLengths[i] = 1;
			}

			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundUnitSizeImages, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		//Bounded-size images: s_j \elem [s_min, s_max] for some constants s_min and s_max
		static Dictionary<string, decimal> cLowerBoundBoundedSizeImages = new();
		public static (float, float) BoundedSizeImages(int imageCount, int blackoutCount, decimal sMinBound, decimal sMaxBound, Action<int, decimal[], IOnlineTimeline> f)
		{
			if (sMaxBound < sMinBound)
				throw new InvalidCastException();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount, sMinBound, sMaxBound);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundBoundedSizeImages, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Only one unavailable interval: m = 1
		public static Dictionary<string, decimal> cLowerBoundOneBlackout = new();
		public static (float, float) OnlyOneBlackout(int imageCount, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(1);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(1, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, 1, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundOneBlackout, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic unavailable intervals: for all i, the i-th unavailable interval has t_i = i * p for some p
		public static Dictionary<string, decimal> cLowerBoundBlackoutsWithInterfalls = new();
		public static (float, float) BlackoutsWithInterfalls(int imageCount, int blackoutCount, decimal p, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundBlackoutsWithInterfalls, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Periodic and regular unavailable intervals: for all i, the i-th unavailable interval has ti = i * p for some p and l_i = l
		public static Dictionary<string, decimal> cLowerBoundBlackoutsWithInterfallsAndLengths = new();
		public static (float, float) BlackoutsWithInterfallsAndLengths(int imageCount, int blackoutCount, decimal p, decimal l, Action<int, decimal[], IOnlineTimeline> f)
		{
			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.CreateBlackoutsWithInterval(blackoutCount, p, l);

			if (imageCount > 2 && blackoutCount > 2 && p > 0 && l > 0 && p > l)
			{
				//asdf
			}

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundBlackoutsWithInterfallsAndLengths, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}

		// Example from https://uu.blackboard.com/bbcswebdav/pid-4269249-dt-content-rid-56319541_2/courses/BETA-2022-1-GS-INFOMADS-V/Telescope_scheduling%20%281%29.pdf
		// Bounded-length unavailable intervals: for all i, the i-th unavailable inter-val has l_i \elem [l_min, l_max] for some constants l_min and l_max
		public static Dictionary<string, decimal> cLowerBoundBoundedLengthBlackouts = new();
		public static (float, float) BoundedLengthBlackouts(int imageCount, int blackoutCount, decimal lowerbound, decimal upperbound, Action<int, decimal[], IOnlineTimeline> f)
		{
			if (upperbound < lowerbound)
				throw new InvalidCastException();

			decimal[] imageLengths = TestHelpers.RandomImages(imageCount);
			decimal[] blackoutStartAndLength = TestHelpers.RandomBlackouts(blackoutCount, lowerbound, upperbound);

			Timeline onlineResult = TestHelpers.CreateBaseTimeline(blackoutCount, blackoutStartAndLength, imageCount);
			f(imageCount, imageLengths, onlineResult);

			Timeline offlineResult = OfflineAlgorithms.FindOptimalSolutionWithPermutations(imageCount, imageLengths, blackoutCount, blackoutStartAndLength);
			TestHelpers.AddResultToUperbound(cLowerBoundBoundedLengthBlackouts, onlineResult.Length, offlineResult.Length, f);

			TestHelpers.ValidateTimeline(onlineResult, imageCount, imageLengths);

			return ((float)onlineResult.Length, (float)offlineResult.Length);
		}
	}
}
