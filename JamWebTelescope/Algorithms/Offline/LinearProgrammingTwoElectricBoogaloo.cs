﻿using Google.OrTools.LinearSolver;
using JamWebTelescope.entities;
using System.Runtime.CompilerServices;

namespace JamWebTelescope.Algorithms.Offline
{
	public static partial class OfflineAlgorithms
	{
		static public Timeline SearchWithLinearProgramming(
			int amountOfTransmission, // = n
			decimal[] transmissionsLength,
			int amountOfBlackouts, // = m
			decimal[] blackoutStartAndLength)
		{
			return SearchWithLinearProgramming(amountOfTransmission, transmissionsLength, amountOfBlackouts, blackoutStartAndLength, 5 * 60 * 1000);
		}

		static public Timeline SearchWithLinearProgramming(
			int amountOfTransmission, // = n
			decimal[] transmissionsLength,
			int amountOfBlackouts, // = m
			decimal[] blackoutStartAndLength,
			int deadlineMiliseconds = 5 * 60 * 1000)
		{
			// There will be excactly amountOfBlackouts blackouts.
			List<Blackout> blackoutEvents = new List<Blackout>(amountOfBlackouts);
			for (int i = 0; i < amountOfBlackouts; i++)
				blackoutEvents.Add(new Blackout(blackoutStartAndLength[i * 2], blackoutStartAndLength[i * 2 + 1]));

			blackoutEvents.Sort();

			// Create the index list. 
			int[] transmissionIndexList = new int[amountOfTransmission];
			for (int i = 0; i < transmissionIndexList.Length; i++)
				transmissionIndexList[i] = i;

			double totalTransmissionLength = (double)transmissionsLength.Sum();

			// Const maxTime (latest possible time possible)
			double maxTime = 0;
			if (blackoutEvents.Count > 0)
				maxTime += (double)blackoutEvents.Last().End;
			maxTime += totalTransmissionLength;

			// Create solver.
			string solverName = "SCIP";
			Solver solver = Solver.CreateSolver(solverName);
			//CpModel solver = new CpModel();
			if (solver is null)
				throw new Exception("Couldn't create " + solverName + " solver.");

			// Set the maximum of threads to 4
			//solver.SetNumThreads(4);

			solver.SetSolverSpecificParametersAsString("lp/scaling = 0");
			solver.SetSolverSpecificParametersAsString("lp/disablecutoff = 1");
			solver.SetSolverSpecificParametersAsString("heuristics/rins/minnodes = 25");
			solver.SetSolverSpecificParametersAsString("lp/solvefreq = 0");
			solver.SetSolverSpecificParametersAsString("heuristics/fracdiving/freqofs = 1");
			solver.SetSolverSpecificParametersAsString("heuristics/veclendiving/freq = -1");
			solver.SetSolverSpecificParametersAsString("branching/preferbinary = TRUE");
			//solver.SetSolverSpecificParametersAsString("branching/clamp = 0.5"); // minimal relative distance of branching point to bounds when branching on a continuous variable
			solver.SetSolverSpecificParametersAsString("lp/checkdualfeas = FALSE"); // should LP solutions be checked for dual feasibility, resolving LP when numerical troubles occur?
																					//solver.SetSolverSpecificParametersAsString("timing/rareclockcheck  = TRUE");  //should clock checks of solving time be performed less frequently (note: time limit could be exceeded slightly)

			Timeline timeline = new Timeline(blackoutEvents, amountOfTransmission + amountOfBlackouts);
			amountOfBlackouts = blackoutEvents.Count; // Overlapping blackouts will be merged by the timeline.

			// earlier_i,j = File i uploaded after file j (binary)
			Variable[,] fileUploadedAfterFile = new Variable[amountOfTransmission, amountOfTransmission];
			for (int i = 0; i < amountOfTransmission; i++)
				for (int j = 0; j < i; j++)
					fileUploadedAfterFile[i, j] = solver.MakeBoolVar("earlier_" + i + "_" + j);

			Variable[,] fileUploadedAfterBlackout;
			fileUploadedAfterBlackout = solver.MakeBoolVarMatrix(amountOfTransmission, amountOfBlackouts, "earlierthenblackout_");

			//Last end is the time of the ending of the last transmission transmissioned time
			Variable lastEnd = solver.MakeNumVar(0, maxTime, "lastEnd");

			// Start time of all transmissions.
			Variable[] startTransmission = solver.MakeNumVarArray(amountOfTransmission, 0, maxTime);


			// Constraint

			// lastEnd >= start_i + length_i
			for (int i = 0; i < amountOfTransmission; i++)
			{
				solver.Add(lastEnd >= startTransmission[i] + (double)transmissionsLength[i]);
			}

			// Look for lowerbound by checking what blackouts must be in the transmission.
			double totalTransmissionLengthPlusblackoutsThatMustBeInFinalTransmission = totalTransmissionLength;
			foreach (Blackout b in blackoutEvents)
			{
				if ((double)b.Start < totalTransmissionLengthPlusblackoutsThatMustBeInFinalTransmission)
					totalTransmissionLengthPlusblackoutsThatMustBeInFinalTransmission += (double)b.Length;
			}

			solver.Add(lastEnd >= totalTransmissionLengthPlusblackoutsThatMustBeInFinalTransmission);

			Console.Write(".");

			// transmission doesnt fall in a transmission
			for (int i = 0; i < amountOfTransmission; i++)
			{

				for (int j = 0; j < i; j++)
				{
					solver.Add(startTransmission[i] + (double)transmissionsLength[i] <= startTransmission[j] + (maxTime * fileUploadedAfterFile[i, j])); // i eerder dan j
					solver.Add(startTransmission[j] + (double)transmissionsLength[j] <= startTransmission[i] + (maxTime * (1 - fileUploadedAfterFile[i, j]))); // j eerder dan i

					// Order of transmission for files with equal size doesn't matter. So ensure that the lowest index is first.
					if (transmissionsLength[i] == transmissionsLength[j])
						solver.Add(fileUploadedAfterFile[i, j] == 1);
				}
			}

			// earlier_i,j = File i uploaded after blackout j (binary)
			for (int i = 0; i < amountOfTransmission; i++)
			{

				// If a transmission doesn't fit before a blackout it should be after the blackout.
				if (transmissionsLength[i] > blackoutEvents[0].Start)
				{
					solver.Add(fileUploadedAfterBlackout[i, 0] == 1);
					for (int j = 1; j < amountOfBlackouts; j++)
					{
						if (transmissionsLength[i] + blackoutEvents[j - 1].End > blackoutEvents[j].Start)
						{
							solver.Add(fileUploadedAfterBlackout[i, j] == 1);
						}
						else
						{
							break;
						}
					}
				}

				for (int j = 0; j < amountOfBlackouts; j++)
				{
					// Transmission can't fall in a blackout
					solver.Add(startTransmission[i] + (double)transmissionsLength[i] <= (double)blackoutEvents[j].Start + (maxTime * fileUploadedAfterBlackout[i, j])); // i eerder dan j
					solver.Add(startTransmission[i] + (maxTime * (1 - fileUploadedAfterBlackout[i, j])) >= (double)blackoutEvents[j].End); // i eerder dan j

					// If a file is after a blackout it should be after all sequantal blackouts. 
					if (j < amountOfBlackouts - 1)
					{
						solver.Add(fileUploadedAfterBlackout[i, j] >= fileUploadedAfterBlackout[i, j + 1]);
					}

					// If a file is after a blackout, the files after this file should also be after this blackout.
					for (int k = 0; k < i; k++)
					{
						LinearConstraint lc =
							fileUploadedAfterBlackout[i, j] + (1 - fileUploadedAfterFile[i, k]) - 1 <= fileUploadedAfterBlackout[k, j];
						solver.Add(lc);
					}
				}
			}

			Console.Write(".");

			// Set some Constraints as lazy to speed up the solving.
			MPConstraintVector mpcv = solver.constraints();
			if (mpcv.Count < 50000)
				foreach (Constraint c in mpcv)
				{
					bool doneSomething = false;
					for (int i = 0; i < amountOfTransmission; i++)
					{

						if (c.GetCoefficient(startTransmission[i]) != 0)
						{
							c.SetIsLazy(true);
							break;
						}

						for (int j = 0; j < amountOfBlackouts; j++)
						{

							if (c.GetCoefficient(fileUploadedAfterBlackout[i, j]) != 0)
							{
								c.SetIsLazy(true);
								doneSomething = true;
								break;
							}
						}

						if (doneSomething)
							break;
					}
				}

			Console.Write(".");

			// Objective
			solver.Minimize(lastEnd);

			// Timelimit
			solver.SetTimeLimit(deadlineMiliseconds);

			// Check result status.
			var resultStatus = solver.Solve();

			if (resultStatus != Solver.ResultStatus.OPTIMAL)
			{
				if (resultStatus == Solver.ResultStatus.FEASIBLE)
					TestComp.PrintRed("Timelimit hit! " + resultStatus);
				TestComp.PrintRed(resultStatus.ToString());
				timeline.Valid = false;
			}

			for (int i = 0; i < amountOfTransmission; i++)
			{
				double startTime = startTransmission[i].SolutionValue();
				timeline.AddTransmission(new Transmission(i, transmissionsLength[i]), startTime);
			}

			return timeline;
		}

	}
}
