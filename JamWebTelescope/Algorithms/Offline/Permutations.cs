﻿using JamWebTelescope.entities;

namespace JamWebTelescope.Algorithms.Offline
{
	public static partial class OfflineAlgorithms
	{
		static public Timeline FindOptimalSolutionWithPermutations(
			int amountOfTransmission,
			decimal[] transmissionsLength,
			int amountOfBlackouts,
			decimal[] blackoutStartAndLength)
		{
			//if (amountOfTransmission > 9)
			//	throw new Exception("This will take to long or crash your pc");

			// There will be excactly amountOfBlackouts blackouts.
			List<Blackout> blackoutEvents = new List<Blackout>(amountOfBlackouts);
			for (int i = 0; i < amountOfBlackouts; i++)
				blackoutEvents.Add(new Blackout(blackoutStartAndLength[i * 2], blackoutStartAndLength[i * 2 + 1]));

			// Create all possible permutations of the index list. 
			int[] transmissionIndexList = new int[amountOfTransmission];
			for (int i = 0; i < transmissionIndexList.Length; i++)
				transmissionIndexList[i] = i;

			List<int[]> allPermutationsOfTheIndexList = Permutations(new List<int[]>(), transmissionIndexList, 0, amountOfTransmission - 1);

			// Create all possible timelines with the permutations list. Just take the shortest one.
			// This is the best solutions for our online algorithm
			List<Timeline> timelineList = new List<Timeline>(allPermutationsOfTheIndexList.Count);

			foreach (int[] permutation in allPermutationsOfTheIndexList)
			{
				// Create the initial timeline from the input.
				Timeline timeline = new Timeline(blackoutEvents, amountOfBlackouts + amountOfTransmission);

				foreach (int index in permutation)
				{
					timeline.AddTransmission(new Transmission(index, transmissionsLength[index]));
				}

				timelineList.Add(timeline);
			}

			if (timelineList.Count > 0)
			{
				// Find best timeline
				Timeline lowestTimeline = timelineList[0];
				foreach (Timeline timeline in timelineList)
				{
					if (timeline.Length < lowestTimeline.Length)
						lowestTimeline = timeline;
				}

				// Done!
				return lowestTimeline;
			}

			Timeline emptyTimeline = new Timeline(blackoutEvents, amountOfBlackouts + amountOfTransmission);
			return emptyTimeline;
		}

		static public List<int[]> Permutations(List<int[]> excistingPermutations, int[] current, int start, int end)
		{
			if (start == end)
			{
				int[] newCopyOfArray = new int[current.Length];
				current.CopyTo(newCopyOfArray, 0);
				excistingPermutations.Add(newCopyOfArray);
				return excistingPermutations;
			}

			for (int i = start; i <= end; i++)
			{
				// Swap start with the new end. 
				Swap(current, start, i);

				excistingPermutations = Permutations(excistingPermutations, current, start + 1, end);

				// Reset to previous
				Swap(current, start, i);
			}

			// Done!
			return excistingPermutations;
		}

		static void Swap(int[] integerArray, int a, int b)
		{
			int aTemp = integerArray[a];
			int bTemp = integerArray[b];
			integerArray[a] = bTemp;
			integerArray[b] = aTemp;
		}
	}
}
