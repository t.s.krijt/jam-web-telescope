﻿using JamWebTelescope.entities;

namespace JamWebTelescope.Algorithms.Online
{
	public partial class OnlineAlgorithms
	{
		public static void BiggestFirst(
			int amountOfTransmissions,
			decimal[] transmissionsLength,
			IOnlineTimeline onlineTimeline
			)
		{
			List<Transmission> transmissions = new List<Transmission>(amountOfTransmissions);

			for (int i = 0; i < amountOfTransmissions; i++)
				transmissions.Add(new Transmission(i, transmissionsLength[i]));

			transmissions.Sort();
			int index = 0;

			do
			{
				if (index >= amountOfTransmissions)
					break;

				if (onlineTimeline.CanSend())
				{
					bool transmissionSucceeded = onlineTimeline.TryToSend(transmissions[index].Clone());
					if (transmissionSucceeded)
						index++;
				}
			}
			while (onlineTimeline.Step() || index < amountOfTransmissions);
		}
	}
}
