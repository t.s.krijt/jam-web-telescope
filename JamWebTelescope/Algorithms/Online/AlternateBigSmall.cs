﻿using JamWebTelescope.entities;

namespace JamWebTelescope.Algorithms.Online
{
	public partial class OnlineAlgorithms
	{
		public static void AlternateBigSmall(
			int amountOfTransmissions,
			decimal[] transmissionsLength,
			IOnlineTimeline onlineTimeline
			)
		{
			List<Transmission> transmissions = new List<Transmission>(amountOfTransmissions);

			for (int i = 0; i < amountOfTransmissions; i++)
				transmissions.Add(new Transmission(i, transmissionsLength[i]));

			transmissions.Sort();
			bool trySendBig = true;
			int indexStart = 0;
			int indexEnd = transmissions.Count - 1;

			do
			{
				if (indexStart > indexEnd)
					break;

				if (onlineTimeline.CanSend())
				{
					if (trySendBig)
					{
						bool transmissionSucceeded = onlineTimeline.TryToSend(transmissions[indexStart].Clone());
						if (transmissionSucceeded)
							indexStart++;
						trySendBig = !trySendBig;
					}
					else
					{
						bool transmissionSucceeded = onlineTimeline.TryToSend(transmissions[indexEnd].Clone());
						if (transmissionSucceeded)
							indexEnd--;
						trySendBig = !trySendBig;
					}
				}
			}
			while (onlineTimeline.Step() || indexStart < indexEnd);
		}
	}
}
