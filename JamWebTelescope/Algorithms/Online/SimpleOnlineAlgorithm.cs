﻿using JamWebTelescope.entities;

namespace JamWebTelescope.Algorithms.Online
{
	public partial class OnlineAlgorithms
	{
		public static void SimpleOnlineAlgorithm(
			int amountOfTransmissions,
			decimal[] transmissionsLength,
			IOnlineTimeline onlineTimeline
			)
		{
			List<int> succeededTransmissions = new List<int>(amountOfTransmissions);
			do
			{
				if (onlineTimeline.CanSend())
				{
					for (int i = 0; i < amountOfTransmissions; i++)
					{
						if (!succeededTransmissions.Contains(i))
						{
							bool transmissionSucceeded = onlineTimeline.TryToSend(new Transmission(i, transmissionsLength[i]));
							if (transmissionSucceeded)
								succeededTransmissions.Add(i);
							break;
						}
					}
				}
			}
			while (onlineTimeline.Step() || succeededTransmissions.Count < amountOfTransmissions);
		}
	}
}
