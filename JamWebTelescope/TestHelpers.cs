﻿

using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.Algorithms.Online;
using JamWebTelescope.entities;
using System.Diagnostics;
using System.Text;

namespace JamWebTelescope
{
	static public class TestHelpers
	{
		private static Random GetRandom()
		{
			return new Random(693426932);
		}

		public static decimal[] CreateBlackoutsWithInterval(int count, decimal period)
		{
			Random random = GetRandom();
			decimal[] blackouts = new decimal[count * 2];
			for (int i = 0; i < count; i++)
			{
				blackouts[i * 2] = i * period;
				blackouts[i * 2 + 1] = Math.Round((decimal)random.NextDouble() * 10, 5);
			}
			return blackouts;
		}

		public static decimal[] CreateBlackoutsWithInterval(int count, decimal period, decimal length)
		{
			decimal[] blackouts = new decimal[count * 2];
			for (int i = 0; i < count; i++)
			{
				blackouts[i * 2] = i * period;
				blackouts[i * 2 + 1] = length;
			}
			return blackouts;
		}

		public static decimal[] RandomImages(int count, decimal lowerBound = 1, decimal upperBound = 100)
		{
			Random random = GetRandom();
			//Assert.That(upperBound, Is.GreaterThanOrEqualTo(lowerBound));
			decimal multiplicationValue = upperBound - lowerBound;
			decimal[] imageLengths = new decimal[count];
			for (int i = 0; i < count; i++)
			{
				imageLengths[i] = lowerBound + Math.Round(Math.Round((decimal)random.NextDouble() * multiplicationValue, 5), 5);
			}
			return imageLengths;
		}

		public static decimal[] RandomBlackouts(int count, decimal lowerBound = 0, decimal upperBound = 100)
		{
			Random random = GetRandom();
			//Assert.That(upperBound, Is.GreaterThanOrEqualTo(lowerBound));
			decimal multiplicationValue = upperBound - lowerBound;
			decimal[] blackoutStartAndLength = new decimal[count * 2];
			for (int i = 0; i < count * 2; i += 2)
			{
				blackoutStartAndLength[i] = Math.Round((decimal)random.NextDouble() * 100, 5);
				blackoutStartAndLength[i + 1] = lowerBound + Math.Round((decimal)random.NextDouble() * multiplicationValue, 5);
			}
			return blackoutStartAndLength;
		}

		public static Timeline CreateBaseTimeline(int amountOfBlackouts, decimal[] blackoutStartAndLength, int amountOfTransmissions)
		{
			// There will be excactly amountOfBlackouts blackouts.
			List<Blackout> blackoutEvents = new List<Blackout>(amountOfBlackouts);
			for (int i = 0; i < amountOfBlackouts; i++)
				blackoutEvents.Add(new Blackout(blackoutStartAndLength[i * 2], blackoutStartAndLength[i * 2 + 1]));

			// Create the initial timeline from the input.
			return new Timeline(blackoutEvents, amountOfBlackouts + amountOfTransmissions);
		}

		public static void AddResultToUperbound(Dictionary<string, decimal> cUpperbound, decimal result, decimal optimal, Action<int, decimal[], IOnlineTimeline> f)
		{
			if (optimal != 0)
			{
				decimal c = result / optimal;
				if (cUpperbound.ContainsKey(f.Method.Name))
				{
					if (cUpperbound[f.Method.Name] < c)
						cUpperbound[f.Method.Name] = c;
				}
				else
				{
					cUpperbound.Add(f.Method.Name, c);
				}
			}
		}

		public static void ValidateTimeline(Timeline timeline, int TransmissionCount, decimal[] transmissionLegths)
		{
			//if (timeline == null)
			//	Assert.Fail();
#pragma warning disable CS8602 // Dereference of a possibly null reference.

			string[] output = timeline.GetPrintValues(TransmissionCount);

			//Assert.That(TransmissionCount + 1, Is.EqualTo(output.Length));

			foreach (string s in output)
			{
				//Assert.That(s, Is.Not.Null);
				//decimal _ = decimal.Parse(s);
			}

			if (Math.Round(timeline.Length) < Math.Round(transmissionLegths.Sum()))
				TestComp.PrintRed("Faster than possible?");

#pragma warning restore CS8602 // Dereference of a possibly null reference.
		}

		// Read the test from the instances folder
		public static void RunProvidedInstances()
		{
			Dictionary<string, string[]> instanceTestsResults = new();

			string fullPath = Path.GetFullPath("..\\..\\..\\..\\..\\Instances");
			string[] groups = Directory.GetDirectories(fullPath);

			foreach (string group in groups)
			{
				string[] instances = Directory.GetFiles(group);

				foreach (string instance in instances)
				{
					IEnumerator<string> input = File.ReadLines(instance).GetEnumerator();
					input.MoveNext();

					// Read the amount of transmissions.
					int amountOfTransmission = int.Parse(input.Current, System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
					input.MoveNext();

					if (amountOfTransmission >= 11)
						continue;

					Console.Write("Amount of transmissions: " + amountOfTransmission + "   ");

					// Read all transmission lengths. 
					decimal[] transmissionsLength = new decimal[amountOfTransmission];
					for (int i = 0; i < amountOfTransmission; i++)
					{
						transmissionsLength[i] = decimal.Parse(input.Current, System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
						input.MoveNext();
					}

					// Read the amount of Blackouts.
					int amountOfBlackouts = int.Parse(input.Current, System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
					input.MoveNext();

					Console.Write("Amount of blackouts: " + amountOfBlackouts + "   ");

					decimal[] blackoutsStartAndLength = new decimal[amountOfBlackouts * 2];

					for (int i = 0; i < amountOfBlackouts * 2; i += 2)
					{
						string blackoutInput = input.Current;
						input.MoveNext();

						string[] blackoutInputSplit = blackoutInput.Split(",");
						blackoutsStartAndLength[i] = decimal.Parse(blackoutInputSplit[0], System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
						blackoutsStartAndLength[i + 1] = decimal.Parse(blackoutInputSplit[1], System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
					}

					// Test MILP
					int millisecondDeadlineMILP = 5 * 60 * 1000;// 5 * 1000
					Console.Write("Started");
					Stopwatch milp_stopwatch = Stopwatch.StartNew();

					//Timeline result = new Timeline(null, 1);
					Timeline result = OfflineAlgorithms.SearchWithLinearProgramming(amountOfTransmission, transmissionsLength, amountOfBlackouts, blackoutsStartAndLength, millisecondDeadlineMILP);
					
					string instanceResult = input.Current;
					//Timeline result = null;

					//Timeline tmp = result;
					//try
					//{
					//	result = new Timeline(null, 0);
					//	result.Add(new Transmission(0, decimal.Parse(instanceResult, System.Globalization.CultureInfo.GetCultureInfo("en-gb"))));
					//}
					//catch
					//{
					//	result = tmp;
					//	result.Valid = false;
					//}


					milp_stopwatch.Stop();
					string milpOutputValue = "DNF";
					if (result.Valid)
					{
						milpOutputValue = milp_stopwatch.Elapsed.Seconds.ToString() + "." + (Math.Round((float)milp_stopwatch.ElapsedMilliseconds / 1000, 3).ToString().Split('.')[1]).ToString() + "s";
					}

					Stopwatch permutation_stopwatch = Stopwatch.StartNew();
					Timeline result2 = OfflineAlgorithms.FindOptimalSolutionWithPermutations(amountOfTransmission, transmissionsLength, amountOfBlackouts, blackoutsStartAndLength);
					permutation_stopwatch.Stop();
					string permutationOutputValue = "_";
					try
					{
						permutationOutputValue = permutation_stopwatch.Elapsed.Seconds.ToString() + "." + (Math.Round((float)permutation_stopwatch.ElapsedMilliseconds / 1000, 3).ToString().Split('.')[1]).ToString() + "s";
					}
					catch
					{
						permutationOutputValue = "0s";
					}
					Console.WriteLine("   Done!");

					//// Test biggest first
					//Timeline onlineResultBF = TestHelpers.CreateBaseTimeline(amountOfBlackouts, blackoutsStartAndLength, amountOfTransmission);
					//OnlineAlgorithms.BiggestFirst(amountOfTransmission, transmissionsLength, onlineResultBF);
					//decimal onlineResultValueBF = onlineResultBF.Length;
					//bool isOptimalBF = onlineResultValueBF <= result.Length;

					//// Test alternate big small
					//Timeline onlineResultABS = TestHelpers.CreateBaseTimeline(amountOfBlackouts, blackoutsStartAndLength, amountOfTransmission);
					//OnlineAlgorithms.AlternateBigSmall(amountOfTransmission, transmissionsLength, onlineResultABS);
					//decimal onlineResultValueABS = onlineResultABS.Length;
					//bool isOptimalABS = onlineResultValueABS <= result.Length;

					//// Always Alternate Big Small
					//Timeline onlineResultAABF = TestHelpers.CreateBaseTimeline(amountOfBlackouts, blackoutsStartAndLength, amountOfTransmission);
					//OnlineAlgorithms.AlwaysAlternateBigSmall(amountOfTransmission, transmissionsLength, onlineResultAABF);
					//decimal onlineResultValueAABS = onlineResultAABF.Length;
					//bool isOptimalAABS = onlineResultValueAABS <= result.Length;

					//// SmallestFirst
					//Timeline onlineResultSF = TestHelpers.CreateBaseTimeline(amountOfBlackouts, blackoutsStartAndLength, amountOfTransmission);
					//OnlineAlgorithms.SmallestFirst(amountOfTransmission, transmissionsLength, onlineResultSF);
					//decimal onlineResultValueSF = onlineResultSF.Length;
					//bool isOptimalSF = onlineResultValueSF <= result.Length;

					//// In order of index
					//Timeline onlineResultOOF = TestHelpers.CreateBaseTimeline(amountOfBlackouts, blackoutsStartAndLength, amountOfTransmission);
					//OnlineAlgorithms.SimpleOnlineAlgorithm(amountOfTransmission, transmissionsLength, onlineResultOOF);
					//decimal onlineResultValueOOF = onlineResultOOF.Length;
					//bool isOptimalOOF = onlineResultValueOOF <= result.Length;

					string experimentName = instance.Remove(0, fullPath.Length);

					instanceTestsResults.Add(experimentName,
						new string[]
						{
							milpOutputValue,
							permutationOutputValue,
							//onlineResultValueBF.ToString(),
							//onlineResultValueABS.ToString(),
							//onlineResultValueAABS.ToString(),
							//onlineResultValueSF.ToString(),
							//onlineResultValueOOF.ToString(),
					}
						);

					// Make bold if optimal
					//if (isOptimalBF)
					//	instanceTestsResults[experimentName][1] = "\\textbf{" + instanceTestsResults[experimentName][1] + "}";
					//if (isOptimalABS)
					//	instanceTestsResults[experimentName][2] = "\\textbf{" + instanceTestsResults[experimentName][2] + "}";
					//if (isOptimalAABS)
					//	instanceTestsResults[experimentName][3] = "\\textbf{" + instanceTestsResults[experimentName][3] + "}";
					//if (isOptimalSF)
					//	instanceTestsResults[experimentName][4] = "\\textbf{" + instanceTestsResults[experimentName][4] + "}";
					//if (isOptimalOOF)
					//	instanceTestsResults[experimentName][5] = "\\textbf{" + instanceTestsResults[experimentName][5] + "}";


					string[] resultText = result.GetPrintValues(amountOfTransmission);

					//if (result.Valid)
					//	for (int i = 0; i < resultText.Length; i++)
					//	{

					//		double instanceResultNumber = Math.Round(double.Parse(instanceResult, System.Globalization.CultureInfo.GetCultureInfo("en-gb")), 5);
					//		double ourResult = Math.Round(double.Parse(resultText[i], System.Globalization.CultureInfo.GetCultureInfo("en-gb")), 5);

					//		// The exact location of message doesn't need to be the same.
					//		bool firstIsEqual = instanceResultNumber == ourResult;


					//		if (i == 0)
					//		{   // Only the length (:
					//			if (firstIsEqual)
					//				TestComp.PrintGreen("Passed: " + instance);
					//			else
					//			{
					//				TestComp.PrintRed("Failed: " + instance);
					//				Console.WriteLine("Expected: " + instanceResult + " but was " + resultText[i]);
					//			}
					//		}

					//		input.MoveNext();
					//	}


					//TestHelpers.ValidateTimeline(result, amountOfTransmission, transmissionsLength);
				}
			}

			StringBuilder testResults = new StringBuilder();
			foreach (var keyValue in instanceTestsResults)
			{
				testResults.Append("  ");

				string cleanName = keyValue.Key.Replace('\\', ' ').Replace('_', ' ').ToLowerInvariant();
				cleanName = cleanName.Remove(cleanName.Length - 4);
				cleanName = cleanName.Remove(0, 11);
				cleanName = cleanName.Replace("instance", null);

				if (cleanName.Length > 27)
					cleanName = cleanName.Remove(24) + "(...)";

				testResults.Append(cleanName);

				foreach (string s in keyValue.Value)
				{
					testResults.Append(" & ");
					testResults.Append(s);
				}
				testResults.Append("\\\\\n");
			}
			string outputResultsFile = testResults.ToString();

			Task t = File.WriteAllTextAsync(fullPath + "\\..\\OutputFile.txt", outputResultsFile);

			Console.Write(outputResultsFile);
			t.Wait();
		}
	}
}
