﻿using JamWebTelescope.Algorithms.Offline;
using JamWebTelescope.Algorithms.Online;
using JamWebTelescope.entities;

namespace JamWebTelescope // Note: actual namespace depends on the project name.
{
	public static class Program
	{
		static void Main(string[] args)
		{
			// Parse arguments
			for (int i = 0; i < args.Length; i++)
			{
				args[i] = args[i].ToLower();
			}

			bool isOnline = args.Contains("-online");
			bool isOfline = args.Contains("-ofline");
			bool testComp = args.Contains("-testcomp");
			bool testInstances = args.Contains("-instances");

			// TESTING
			if (testComp)
			{
				TestComp.TestCompetitiveness();
				TestComp.Print();
				Console.WriteLine("Tested... Waiting for input");
			}

			// Instances testing
			if (testInstances)
			{
				TestHelpers.RunProvidedInstances();
				Console.WriteLine("Tested... Waiting for input");
			}

			// If neither online nor offline is selected, we run online.
			if (isOfline && isOnline)
				throw new ArgumentException("It can't be ofline and online!");
			else if (isOfline == isOnline)
				isOfline = true;

			// Read the amount of transmissions.
			int amountOfTransmission = int.Parse(Console.ReadLine(), System.Globalization.CultureInfo.GetCultureInfo("en-gb"));

			// Read all transmission lengths. 
			decimal[] transmissionsLength = new decimal[amountOfTransmission];
			for (int i = 0; i < amountOfTransmission; i++)
				transmissionsLength[i] = decimal.Parse(Console.ReadLine(), System.Globalization.CultureInfo.GetCultureInfo("en-gb"));

			// Read the amount of Blackouts.
			int amountOfBlackouts = int.Parse(Console.ReadLine(), System.Globalization.CultureInfo.GetCultureInfo("en-gb"));

			decimal[] blackoutsStartAndLength = new decimal[amountOfBlackouts * 2];

			for (int i = 0; i < amountOfBlackouts * 2; i += 2)
			{
				string? blackoutInput = Console.ReadLine();
				if (blackoutInput != null)
				{
					string[] blackoutInputSplit = blackoutInput.Split(", ");
					blackoutsStartAndLength[i] = decimal.Parse(blackoutInputSplit[0], System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
					blackoutsStartAndLength[i + 1] = decimal.Parse(blackoutInputSplit[1], System.Globalization.CultureInfo.GetCultureInfo("en-gb"));
				}
			}

			Timeline solution = null;
			if (isOnline)
			{
				List<Blackout> blackoutEvents = new List<Blackout>(amountOfBlackouts);
				for (int i = 0; i < amountOfBlackouts; i++)
					blackoutEvents.Add(new Blackout(blackoutsStartAndLength[i * 2], blackoutsStartAndLength[i * 2 + 1]));

				solution = new Timeline(blackoutEvents, amountOfTransmission + amountOfBlackouts);
				OnlineAlgorithms.SimpleOnlineAlgorithm(
					amountOfTransmission,
					transmissionsLength,
					solution
					);
			}
			else if (isOfline)
			{
				solution = OfflineAlgorithms.SearchWithLinearProgramming(
					amountOfTransmission,
					transmissionsLength,
					amountOfBlackouts,
					blackoutsStartAndLength
					);
			}

			if (solution != null)
			{
				string[] solutions = solution.GetPrintValues(amountOfTransmission);

				foreach (string s in solutions)
					Console.WriteLine(s);
			}

			Console.ReadLine();
		}




	}
}
